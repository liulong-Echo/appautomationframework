#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'tian'
__data__ = '2022/2/24 16:51'
# software: PyCharm


"""
处理Excel参数
"""

import xlrd
from conf import settings


class ExcelOperate:
    def __init__(self, file_path, sheet_by_index=0):
        self.book = xlrd.open_workbook(file_path)
        self.sheet = self.book.sheet_by_index(sheet_by_index)

    def get_excel(self):
        """
        获取Excel表中接口测试用例
        :return: 构造好的测试用例数据,格式：[{'case_num': '', 'title ': '', 'url': '',}]
        """
        title = self.sheet.row_values(0)
        return [dict(zip(title, self.sheet.row_values(row))) for row in range(1, self.sheet.nrows)]


if __name__ == '__main__':
    obj_excel = ExcelOperate(settings.FILES_PATH, 0)
    excel_data_list = obj_excel.get_excel()
    print(excel_data_list)
