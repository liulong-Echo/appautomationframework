#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'tian'
__data__ = '2022/2/24 17:19'
# software: PyCharm

import random
import time
import os
import shutil
import re

from conf import settings
from utils.loghandler import logger

"""
公共方法类

"""


class PublicTools:

    def __init__(self):
        self.current_time = time.strftime("%Y-%m-%d %H:%M:%S")  # 记录错误日志的时间
        self.report_path = settings.DIR_PATH_DICT.get("report")  # 测试报告目录
        self.screen_path = settings.DIR_PATH_DICT.get("screenshots")  # 截图保存目录
        self.database_path = settings.DIR_PATH_DICT.get("data")  # 测试用例需要的公共数据

    def del_screen(self):
        """
        默认删除10日以前的截图文件夹
        :return:
        """
        try:
            if os.path.isdir(self.screen_path):  # 删除前先判断目录是否存在
                if len(os.listdir(self.screen_path)) > settings.SAVE_DATE:
                    os.chdir(self.screen_path)  # 切换到screenshots目录
                    scr_path = os.getcwd()  # 获取当前目录
                    for d in os.listdir(scr_path):
                        shutil.rmtree(d)  # 删除截图(screenshots)目录下目录和文件
                        if len(os.listdir(scr_path)) <= settings.SAVE_DATE:
                            break
            else:
                print("截图目录不存在")

        except Exception as error:
            logger().info("删除截图文件夹失败:{0}".format(error))
        else:
            return "删除截图文件夹成功"

    def del_report(self):
        """
        默认删除10日以前的测试报告
        :return:
        """
        try:
            if os.path.isdir(self.report_path):
                if len(os.listdir(self.report_path)) > settings.REPORT_SAVE_DATE:
                    os.chdir(self.report_path)  # 切换到report目录
                    report_path = os.getcwd() # 获取当前目录
                    for d in os.listdir(report_path):
                        os.remove(d)
                        if len(os.listdir(report_path)) <= settings.REPORT_SAVE_DATE:
                            break
            else:
                print("删除测试报告文件不存在")
        except Exception as error:
            logger().info("删除测试报告文件夹失败:{0}".format(error))
        else:
            return "删除测试报告成功"


if __name__ == '__main__':
    obj = PublicTools()
    rest_list = obj.del_report()
    print(rest_list)
