#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'tian'
__data__ = '2022/2/24 17:57'
# software: PyCharm


import json

from conf import settings


class ProcessingData:
    """
    读取data中txt和json格式的数据，进行处理后返回
    测试用例中需要参数化的地方，来这里进行调用即可
    """

    def __init__(self, txt_file_name="title", json_file_name="username.json"):
        """
            :param txt_file_name: txt文件名
            :param json_file_name:json文件名
        """
        data_path = settings.DIR_PATH_DICT.get("data")
        self.text_file_name = r"{0}\{1}\{2}".format(data_path, "textcontent", txt_file_name)
        self.json_file_name = r"{0}\{1}\{2}".format(data_path, "jsondata", json_file_name)
        self.txt_data_list = []

    def get_data_txt(self):
        """
        读取txt文件中定义的内容，将内容添加到列表返回
        :return:已列表形式返回读取txt文件中的值
        """
        with open(self.text_file_name, "r+", encoding="utf-8") as fr:
            for i in fr:
                if i.strip() == "":
                    continue
                self.txt_data_list.append(i.strip())
        return self.txt_data_list

    def get_data_json(self):
        """
        处理json
        :return:字典类型的数据
        """
        with open(self.json_file_name, "r+", encoding="utf-8") as fr:
            for i in fr:
                return json.loads(i)


if __name__ == '__main__':
    obj = ProcessingData()
    rest = obj.get_data_json()

