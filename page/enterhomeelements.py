#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author： 青城子
# datetime： 2022/2/28 22:01 
# ide： PyCharm
"""
处理首页页面元素定位&操作,返回实际运行app获取到的值给handled中定义模块
"""
import time
from base.operatingapp import PublicOperation


class EnterHomeElements(PublicOperation):

    def __init__(self):
        super().__init__()
        self.devices.app_stop(package_name=self.pack_name)
        self.devices.app_clear(package_name=self.pack_name)
        self.devices.app_start(package_name=self.pack_name)
        self.for_count = 8  # 由下往上滑动页面默认循环次数
        if not self.devices.uiautomator.running():
            self.devices.uiautomator.start()
        self.start_handle_watcher()

    def start_handle_watcher(self):
        """
        监控器,会单独起一个线程来监控当前页面有没有符合xpath定位元素，如果有就进行相应操作
        :return:
        """
        # app获取获取定位信息
        self.devices.watcher.when(
            '//*[@resource-id="com.lbe.security.miui:id/permission_allow_foreground_only_button"]').click()
        # 省份选择
        self.devices.watcher.when(
            '//*[@resource-id="com.cw.manyhouses:id/recycler_view_km"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]').click()
        # 开启后台启动页面的权限
        self.devices.watcher.when('//*[@resource-id="com.cw.manyhouses:id/tv_left"]').click()
        # 启动监控器
        self.devices.watcher.start()
        # self.devices.watcher.stop()

    def enter_home_element(self):
        """
        :return:
        """

        self.devices(resourceId="com.cw.manyhouses:id/fl_home").click()
        time.sleep(3)
        return self.devices.xpath('//*[@text="公寓"]').get_text()

    def swipe_home_page(self):
        """
        滑动首页查看房源信息
        :return:
        """
        rest_value = ""
        if self.devices.wait_activity(".activity.main.MainActivity", timeout=10):
            while self.for_count > 1:
                self.up_swipe_size()
                self.for_count -= 1
            if self.devices.xpath('//*[@text="显示更多"]').get_text() == "显示更多":
                rest_value = "显示更多"
        return rest_value





