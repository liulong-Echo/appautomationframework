#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author： 青城子
# datetime： 2022/2/28 21:59 
# ide： PyCharm

"""
首页逻辑处理,预期值与page中返回的实际值进行判断，将结果返回给测试用例
"""

from page.enterhomeelements import EnterHomeElements


class EnterHomeHandled:
    def __init__(self):
        self.home_obj = EnterHomeElements()
        self.source_value = "公寓"
        self.source_view_more = "显示更多"

    def enter_home_handled(self):
        """
        进入app默认页面
        :return:
        """
        actual_value = self.home_obj.enter_home_element()
        return self.source_value, actual_value

    def swipe_page_view_data(self):
        """
        首页滑动页面查看房源信息
        :return:
        """
        actual_value = self.home_obj.swipe_home_page()
        return self.source_view_more, actual_value

    def restore_app_initialization(self):
        """
        用例执行完毕后，恢复app到初始化状态
        :return:
        """
        self.home_obj.quit_app()
