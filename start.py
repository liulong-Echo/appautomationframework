#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'tian'
__data__ = '2022/2/24 16:36'
# software: PyCharm

import time
import unittest
import re

from multiprocessing import Process
from adbutils import adb  # 获取连接移动设备编号或ip

from conf import settings
from utils import send_email
from utils.reporthtml.HTMLTestRunner import HTMLTestRunner
from utils.CommonClass import PublicTools
from base.operatingapp import get_devices_list


def run_test_suite():
    """
    运行测试用例集
    :return:
    """
    return unittest.defaultTestLoader.discover(r"./testsuite", pattern="*_test.py")


def institute(serial):
    print("已经到了这里面了", serial)
    get_devices_list(serial)
    if re.search(r"\d+\.*", serial):
        serial = serial.rsplit(":")[0]
    else:
        serial = serial
    now = time.strftime("%Y-%m-%d %H-%M-%S")
    file_name = r"{0}\{1}_{2}_rest.html".format(settings.DIR_PATH_DICT.get("report"), now, serial)
    print(file_name)
    with open(file_name, "wb") as f:
        HTMLTestRunner(
            stream=f,
            title="安卓自动化功能测试报告",
            description="运行移动设备:{0}".format(serial),
            verbosity=2
        ).run(run_test_suite())


def get_devices():
    rest = [d.serial for d in adb.device_list()]
    print(rest)
    return rest


def main():
    for i in range(len(get_devices())):
        serial = get_devices()[i]
        p = Process(target=institute, args=(serial,))
        p.start()


if __name__ == '__main__':
    main()
    # 发送邮件
    # send_email.main_email()
    # 删除10日前的测试报告和截图
    pub_obj = PublicTools()
    pub_obj.del_report()
    pub_obj.del_screen()
