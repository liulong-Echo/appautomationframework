#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'tian'
__data__ = '2022/2/24 16:46'
# software: PyCharm

import os
import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 安卓设备ID
DEVICES_NUMBER = "jra6qcqcjninn7qo"
# 安卓设备IP
DEVICES_IP = "192.168.3.143:5555"
# 默认与被测设备USB连接,False是wifi连接
DEFAULT = False
# 被测项目包名
APP_PACKAGE = "com.cw.manyhouses"

# 测试apk路径
APK_PATH = ""

# 目录路径配置
DIR_PATH_DICT = {
    "report": os.path.join(BASE_DIR, "report"),
    "screenshots": os.path.join(BASE_DIR, "screenshots"),
    "log": os.path.join(BASE_DIR, "logs"),
    "data": os.path.join(BASE_DIR, "database"),
}

# 关于Excel配置表中测试用例默认配置
FILE_NAME = "测试需要参数.xlsx"
FILES_PATH = os.path.join(BASE_DIR, "database", "exceldata", FILE_NAME)

# time.sleep()暂停秒数默认配置
STOP_TIME = 3
# click_sleep()默认配置秒数
CLICK_SLEEP = 3

# 日志相关
# 日志级别
LOG_LEVEL = "debug"
LOG_STREAM_LEVEL = "debug"  # 屏幕输出流
LOG_FILE_LEVEL = "info"  # 文件输出流

# 日志文件命名
LOG_FILE_NAME = os.path.join(BASE_DIR, "logs", datetime.datetime.now().strftime("%Y-%m-%d") + ".logs")


# 截图保存日期，默认10天
SAVE_DATE = 10
# 测试报告
REPORT_SAVE_DATE = 10

