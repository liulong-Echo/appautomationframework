#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author： 青城子
# datetime： 2022/2/28 21:51 
# ide： PyCharm

import unittest

from handled.enterhomehandled import EnterHomeHandled


class EnterHomePage(unittest.TestCase):
    """
    首页功能测试用例
    """
    @classmethod
    def setUpClass(cls):
        cls.handle_obj = EnterHomeHandled()

    def test_01_a_enter_home(self):
        """进入home页面"""
        source_value, actual_value = self.handle_obj.enter_home_handled()
        self.assertEqual(source_value, actual_value, msg="进入home页面失败")

    def test_02_b_view_home_page(self):
        """查看所有房源，进入首页然后由下向上不停滑动直到获取到显示更多为止"""
        source_value, actual_value = self.handle_obj.swipe_page_view_data()
        self.assertEqual(source_value, actual_value, msg="首页由下向上滑动查看房源失败")

    @classmethod
    def tearDownClass(cls):
        cls.handle_obj.restore_app_initialization()
